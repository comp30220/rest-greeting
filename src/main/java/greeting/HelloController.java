package greeting;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/greetings/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return "Hello "+name;
    }
//    public String hello(@RequestParam(value="name",defaultValue="Dude") String name) {
//        return "Hello "+name;
//    }
}
